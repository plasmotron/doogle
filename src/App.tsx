import { useState, useEffect } from "react";

import {
  ComboBox,
  Divider,
  Flex,
  Item,
  Switch,
  TabList,
  TabPanels,
  Tabs,
  View,
  Image,
  InlineAlert,
  Content,
  Heading,
} from "@adobe/react-spectrum";

import { Button } from "@adobe/react-spectrum";
import { Breed, Dog, Pagination } from "./types";
import DogCard from "./components/DogCard";
import { fetchBreeds, fetchDogs } from "./api";
import { MetricContext } from "./contexts/MetricContext";
import { CompareContext } from "./contexts/CompareContext";
import DogFullInfo from "./components/DogFullInfo";

function App() {
  const [dogs, setDogs] = useState<Dog[]>([]);
  const [dogsToCompare, setDogsToCompare] = useState<Dog[]>([]);
  const [breedsCatalog, setBreedsCatalog] = useState<Breed[]>([]);
  const [selectedBreed, setSelectedBreed] = useState<string>("");
  const [currentPage, setCurrentPage] = useState(0);
  const [paginationInfo, setPaginationInfo] = useState<Pagination>();
  const [isMetric, setIsMetric] = useState(true);
  const [hasBreeds, setHasBreeds] = useState(true);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const fetchData = async (selectedKey: string, page: number) => {
      setIsLoading(true);
      const { data, pagination } = await fetchDogs(
        selectedKey,
        hasBreeds,
        page,
        20
      );
      setDogs((prevDogs) => [...prevDogs, ...data]);
      setPaginationInfo(pagination);
      setIsLoading(false);
    };
    fetchData(selectedBreed, currentPage);
  }, [selectedBreed, currentPage, hasBreeds]);

  useEffect(() => {
    const fetchData = async () => {
      const breeds = await fetchBreeds();
      setBreedsCatalog(breeds);
    };

    fetchData();
  }, []);

  const handleSelectionChange = (key: string | number | null) => {
    setDogs([]);
    setCurrentPage(0);
    setSelectedBreed(key?.toString() || "");
  };

  const handleHasBreedsChange = (key: boolean) => {
    setDogs([]);
    setCurrentPage(0);
    setHasBreeds(key);
  };

  return (
    <MetricContext.Provider value={isMetric}>
      <CompareContext.Provider value={[dogsToCompare, setDogsToCompare]}>
        <Flex direction="column" alignItems="center" minWidth={1200}>
          <Flex
            direction="column"
            gap="size-100"
            maxWidth={"1000px"}
            alignItems="center"
            justifyContent="center"
          >
            <Image
              src="/FireflyDoogle.png"
              alt="Doogle"
              width={"size-5000"}
            ></Image>

            <Flex direction="row" alignItems="center" justifyContent="center">
              <View
                borderWidth="thin"
                borderColor="dark"
                borderRadius="medium"
                padding="size-150"
              >
                <Switch isSelected={isMetric} onChange={setIsMetric}>
                  Use Metric
                </Switch>
              </View>
            </Flex>

            <View gridArea="content">
              <Tabs aria-label="Doogle">
                <TabList>
                  <Item key="SearchTab" textValue="SearchTab">
                    Search
                  </Item>
                  <Item key="CompareTab" textValue="CompareTab">
                    Compare ({dogsToCompare.length})
                  </Item>
                </TabList>
                <TabPanels>
                  <Item key="SearchTab" textValue="SearchTab">
                    <Flex direction="row" gap="size-100" alignItems="center">
                      <View paddingTop={"size-350"}>
                        <Switch
                          isSelected={hasBreeds}
                          onChange={handleHasBreedsChange}
                        >
                          Has Breeds
                        </Switch>
                      </View>

                      <ComboBox
                        label="Breed"
                        isDisabled={!hasBreeds}
                        onSelectionChange={(key) => handleSelectionChange(key)}
                        key={"breeds"}
                        defaultInputValue={
                          breedsCatalog.find(
                            (breed) => breed.id.toString() === selectedBreed
                          )?.name || ""
                        }
                      >
                        {breedsCatalog.map((breed) => (
                          <Item key={breed.id} textValue={breed.name}>
                            {breed.name}
                          </Item>
                        ))}
                      </ComboBox>
                    </Flex>

                    <Divider
                      marginTop={"20px"}
                      marginBottom={"20px"}
                      size="M"
                      minWidth={"1000px"}
                    />

                    <Flex direction="row" gap="size-100" wrap>
                      {dogs.map((item) => (
                        <View width={150}>
                          <DogCard key={"dog" + item.id} dog={item} />
                        </View>
                      ))}
                    </Flex>
                    {paginationInfo && (
                      <>
                        <Divider
                          marginTop={"20px"}
                          marginBottom={"20px"}
                          size="M"
                        />
                        <Flex
                          direction="column"
                          gap="size-100"
                          alignItems={"center"}
                        >
                          {isLoading ? (
                            <>Loading...</>
                          ) : (
                            <>
                              Showing {dogs.length} of {paginationInfo.count}
                            </>
                          )}
                          <Button
                            height={"size-100"}
                            variant={"accent"}
                            isDisabled={dogs.length === paginationInfo.count}
                            onPress={() => setCurrentPage(currentPage + 1)}
                            isPending={isLoading}
                          >
                            Show More
                          </Button>
                        </Flex>
                      </>
                    )}
                  </Item>
                  <Item key="CompareTab" textValue="CompareTab">
                    <Divider
                      marginTop={"20px"}
                      marginBottom={"20px"}
                      size="M"
                      minWidth={"1100px"}
                    />
                    <Flex direction="row" gap="size-100" wrap>
                      {dogsToCompare.length > 0 ? (
                        dogsToCompare.map((item) => (
                          <View width={300}>
                            <DogFullInfo key={"dog" + item.id} dog={item} />
                          </View>
                        ))
                      ) : (
                        <InlineAlert alignSelf={"center"} width={"100%"}>
                          <Heading>
                            You havent added any dogs to compare
                          </Heading>
                          <Content>
                            Please add dogs from the "Search tab"
                          </Content>
                        </InlineAlert>
                      )}
                    </Flex>
                  </Item>
                </TabPanels>
              </Tabs>
            </View>
          </Flex>
        </Flex>
      </CompareContext.Provider>
    </MetricContext.Provider>
  );
}

export default App;
