export async function fetchDogs(
  breedId: string,
  has_breeds: boolean,
  page: number,
  limit: number
) {
  const baseUrl = "https://api.thedogapi.com/v1/images/search";
  const params = new URLSearchParams({
    size: "med",
    mime_types: "jpg",
    format: "json",
    order: "ASC",
    limit: limit.toString(),
    page: page.toString(),
    has_breeds: has_breeds.toString(),
  });

  if (breedId && has_breeds) {
    params.append("breed_ids", breedId);
  }

  const url = `${baseUrl}?${params.toString()}`;

  return fetch(url, {
    headers: {
      "x-api-key": `${import.meta.env.VITE_DOGS_API_KEY}`,
    },
  })
    .then((response) => {
      const pagination = {
        count: parseInt(response.headers.get("Pagination-Count") || "0", 10),
        limit: parseInt(response.headers.get("Pagination-Limit") || "0", 10),
        page: parseInt(response.headers.get("Pagination-Page") || "0", 10),
      };
      return response.json().then((data) => ({ data, pagination }));
    })
    .catch((error) => {
      console.error("Failed to fetch data", error);
      return { data: [], pagination: { count: 0, limit: 0, page: 0 } }; // return a default value
    });
}

export async function fetchBreeds() {
  const baseUrl = "https://api.thedogapi.com/v1/breeds";

  const params = new URLSearchParams({
    limit: "1000",
    page: "0",
  });

  const url = `${baseUrl}?${params.toString()}`;

  return fetch(url, {
    headers: {
      "x-api-key": `${import.meta.env.VITE_DOGS_API_KEY}`,
    },
  })
    .then((response) => response.json())
    .catch((error) => console.error("Failed to fetch data", error));
}
