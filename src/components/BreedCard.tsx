import {
  Cell,
  Column,
  Row,
  TableBody,
  TableHeader,
  TableView,
} from "@adobe/react-spectrum";
import { Breed } from "../types";
import { useContext } from "react";
import { MetricContext } from "../contexts/MetricContext";

export const BreedCard = ({ breed }: { breed: Breed }) => {
  const isMetric = useContext(MetricContext);

  return (
    <TableView flex aria-label="Breed information">
      <TableHeader>
        <Column>Property</Column>
        <Column align="end">Value</Column>
      </TableHeader>
      <TableBody>
        <Row>
          <Cell>Name</Cell>
          <Cell>{breed.name}</Cell>
        </Row>
        <Row>
          <Cell>Weight ({isMetric ? "kg" : "lbs"})</Cell>
          <Cell>{isMetric ? breed.weight.metric : breed.weight.imperial}</Cell>
        </Row>
        <Row>
          <Cell>Height ({isMetric ? "cm" : "inches"})</Cell>
          <Cell>{isMetric ? breed.height.metric : breed.height.imperial}</Cell>
        </Row>
        <Row>
          <Cell>Bred for</Cell>
          <Cell>{breed.bred_for}</Cell>
        </Row>
        <Row>
          <Cell>Life span</Cell>
          <Cell>{breed.life_span}</Cell>
        </Row>
        <Row>
          <Cell>Temperament</Cell>
          <Cell>{breed.temperament}</Cell>
        </Row>
        <Row>
          <Cell>Origin</Cell>
          <Cell>{breed.origin}</Cell>
        </Row>
      </TableBody>
    </TableView>
  );
};
