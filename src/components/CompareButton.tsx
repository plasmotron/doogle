import { ToggleButton, Tooltip, TooltipTrigger } from "@adobe/react-spectrum";
import { Dog } from "../types";
import { useContext } from "react";

import { CompareContext } from "../contexts/CompareContext";
import Compare from "@spectrum-icons/workflow/Compare";
import Remove from "@spectrum-icons/workflow/Remove";

type CompareButtonProps = {
  dog: Dog;
  // this is just to be able to position it better in the DogCard without having to use extra CSS
  position?: "relative" | "absolute";
};

export const CompareButton = ({
  dog,
  position = "relative",
}: CompareButtonProps) => {
  const [dogsToCompare, setDogsToCompare] = useContext(CompareContext);

  const isAdded = dogsToCompare.some((compDog) => compDog.id === dog.id);

  const onCompareToggle = () => {
    if (isAdded) {
      setDogsToCompare(
        dogsToCompare.filter((compDog) => compDog.id !== dog.id)
      );
    } else {
      setDogsToCompare([...dogsToCompare, dog]);
    }
  };

  return (
    <>
      <TooltipTrigger delay={0}>
        <ToggleButton
          aria-label="Compare"
          onPress={onCompareToggle}
          margin={"size-50"}
          position={position}
        >
          {isAdded ? <Remove /> : <Compare />}
        </ToggleButton>
        <Tooltip>Add dog to compare</Tooltip>
      </TooltipTrigger>
    </>
  );
};
