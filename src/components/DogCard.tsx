import React from "react";
import {
  Content,
  Dialog,
  DialogTrigger,
  Flex,
  Image,
  Link,
} from "@adobe/react-spectrum";

import { Dog } from "../types";
import { CompareButton } from "./CompareButton";
import DogFullInfo from "./DogFullInfo";

interface DogCardProps {
  size?: "small" | "large";
  dog: Dog;
}

const DogCard: React.FC<DogCardProps> = ({ size = "small", dog }) => {
  const { breeds, url } = dog;

  const width = size === "small" ? "150px" : "300px";

  return (
    <Flex direction="row" gap="size-100" width={width} wrap>
      <Image
        src={url}
        alt={breeds?.[0]?.name || "Unknown"}
        width={width}
        height={width}
        objectFit={"cover"}
      />

      <DialogTrigger isDismissable type="modal">
        {breeds && breeds.length > 0 ? (
          <Link>{breeds[0].name}</Link>
        ) : (
          <Link>Unknown</Link>
        )}

        <Dialog>
          <Content>
            <DogFullInfo dog={dog} size="large" />
          </Content>
        </Dialog>
      </DialogTrigger>

      <CompareButton dog={dog} position="absolute" />
    </Flex>
  );
};

export default DogCard;
