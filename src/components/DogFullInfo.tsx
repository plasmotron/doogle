import { Content, Flex, Image } from "@adobe/react-spectrum";

import { Dog } from "../types";
import { BreedCard } from "./BreedCard";
import { CompareButton } from "./CompareButton";

interface DogFullInfoProps {
  size?: "medium" | "large";
  dog: Dog;
}

const DogFullInfo: React.FC<DogFullInfoProps> = ({ size = "medium", dog }) => {
  const { breeds, url } = dog;
  const alt = breeds?.[0]?.name || "Unknown";

  return (
    <Flex direction="row" gap="size-100" width={"100%"}>
      <Content>
        {size === "large" ? (
          <Image src={url} alt={alt} />
        ) : (
          <Image
            src={url}
            alt={alt}
            width={"300px"}
            height={"300px"}
            objectFit={"cover"}
          />
        )}
        <CompareButton dog={dog} />
        {breeds?.length ?? 0 > 0 ? (
          breeds?.map((breed) => (
            <>
              <h1>{breed.name}</h1>
              <BreedCard breed={breed} />
            </>
          ))
        ) : (
          <p>No breeds info</p>
        )}
      </Content>
    </Flex>
  );
};

export default DogFullInfo;
