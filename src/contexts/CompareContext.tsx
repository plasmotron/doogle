import { createContext, Dispatch, SetStateAction } from "react";
import { Dog } from "../types";

type CompareContextType = [Dog[], Dispatch<SetStateAction<Dog[]>>];

export const CompareContext = createContext<CompareContextType>([[], () => {}]);
